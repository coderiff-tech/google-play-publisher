# google-play-publisher

Google Play Publisher for Android apps.

It's implemented as a .NET 7 web application and provides a single http POST endpoint to upload the *aab* file.
The credentials are not stored in the app and must be injected at runtime as environment variables.

## Pre-Requirements
In order to publish Android apps (i.e: *.aab) you need to have the following pre-requirements:
- A Google Play Console account with an app already configured and ready for new releases
- A Google Cloud Platform account with a Project
- A link between Google Play Console and the Google Cloud Platform's project
- The Google Play Developer API enabled at the Google Play Console
- A Service Account in the Google Cloud Platform's project (it's a special email) with, at least, role *Android Management User*
- The Service Account's email configured at Google Play Console's Users and Permissions (to grant access to manage the account)
- The Keys (JSON format) for the Service Account (it includes all the needed info to be authorized at Google Play Publisher API)

More info at https://developers.google.com/android-publisher/getting_started

## How to use it
This application is designed to run as a docker container, so that it can run as a service as part of a CI/CD pipeline.

With docker:
```
docker run \
  --name google-play-publisher \
  -p 5000:80 \
  -e ServiceAccount__PrivateKeyHex="<your_private_key_hex_format>" \
  -e ServiceAccount__ClientEmail="<your_account_service_email>" \
  -e ServiceAccount__ProjectId="<your_project_id>" \
  -e ServiceAccount__Type="service_account" \
  -e ServiceAccount__PrivateKeyId="<your_private_key_id>" \
  -e ServiceAccount__ClientId="<your_client_id>" \
  registry.gitlab.com/roundev/devops/google-play-publisher:latest
```

Send an http POST request to upload

```
curl -i --data-binary @your_file.aab http://localhost:5000/api/apps/<your_package_name>/tracks/<track_name>
```

There is also a health endpoint at http://localhost:5000/health that could be invoked with
```
curl -i http://localhost:5000/health
```
and would return
```
HTTP/1.1 200 OK
Content-Type: text/plain; charset=utf-8
Date: Sun, 15 Jan 2023 11:46:52 GMT
Server: Kestrel
Transfer-Encoding: chunked

OK
```

### Private key
The private key in a Google's Service Account JSON is a string value that looks like this:
```
"private_key": "-----BEGIN PRIVATE KEY-----\nfoo...\nbar\n-----END PRIVATE KEY-----\n",
```

Although the internal logic expects a private key like that, there are a few problems with passing it around like that:
- Docker run won't handle strings with `\n` characters properly
- GitLab CI/CD variables won't be able to protect values in that format

In order to avoid these problems and allow configuring this private key as a setting or as an environment variable, it's neccessary to encode it
in hexadecimal format by using any tool like https://codebeautify.org/string-hex-converter. The produced result would look like this:
```
2d2d2d2d2d424...b45592d2d2d2d2d5c6e
```
which is a good format to pass around as environment variable or setting, and to protect when using GitLab CI/CD variables.

This value needs to be passed for the key `PrivateKeyHex`, as shown in the Docker example.