using Microsoft.AspNetCore.Server.Kestrel.Core;
using Roundev.GooglePlayPublisher.Factories;
using Roundev.GooglePlayPublisher.Services;

var builder = WebApplication.CreateBuilder(args);

// DI Registrations
var services = builder.Services;
services.AddLogging(configure =>
{
    configure.AddConsole();
});
services.AddSingleton<ServiceAccountCredentialsFactory>();
services.AddSingleton<ReleaseNotesFactory>();
services.AddTransient<PublisherService>();

// See https://github.com/dotnet/aspnetcore/issues/20369
services.Configure<KestrelServerOptions>(options =>
{
    options.Limits.MaxRequestBodySize = int.MaxValue; // if don't set default value is: 30 MB
});

var app = builder.Build();

// Middleware pipeline
app.MapGet("/health", () => "OK");
app.MapPost(
    "api/apps/{packageName}/tracks/{trackName}", 
    async(string packageName, 
        string trackName, 
        Stream aabStream,
        PublisherService publisherService) =>
    {
        await publisherService.UploadApp(packageName, trackName, aabStream);
        TypedResults.Ok("Success");
    });

app.Run();
