using Google.Apis.AndroidPublisher.v3;
using Google.Apis.AndroidPublisher.v3.Data;
using Google.Apis.Auth.OAuth2;
using Roundev.GooglePlayPublisher.Factories;

namespace Roundev.GooglePlayPublisher.Services;

public class PublisherService
{
    private readonly ILogger<PublisherService> _logger;
    private readonly ReleaseNotesFactory _releaseNotesFactory;
    private readonly ServiceAccountCredential _serviceAccountCredential;

    public PublisherService(
        ILogger<PublisherService>  logger,
        ServiceAccountCredentialsFactory serviceAccountCredentialsFactory,
        ReleaseNotesFactory releaseNotesFactory)
    {
        _logger = logger;
        _releaseNotesFactory = releaseNotesFactory;
        _serviceAccountCredential = serviceAccountCredentialsFactory.CreateFromJsonProperties();
    }

    public async Task UploadApp(string packageName, string trackName, Stream stream)
    {
        _logger.LogInformation($"Uploading aab for {packageName} on track {trackName}");
        var service = new AndroidPublisherService();
        var editId = await InsertEdit(service, packageName, trackName, stream);
        await Commit(service, editId, packageName);
    }
    private async Task<string> InsertEdit(AndroidPublisherService service, string packageName, string trackName, Stream aabStream)
    {
        var edit = service.Edits.Insert(new AppEdit { ExpiryTimeSeconds = "3600" }, packageName);
        edit.Credential = _serviceAccountCredential;
        var editInsertResponse = await edit.ExecuteAsync();
        var editId = editInsertResponse.Id;
        _logger.LogInformation($"Edits started with id {editId}");

        var tracksList = service.Edits.Tracks.List(packageName, editInsertResponse.Id);
        tracksList.Credential = _serviceAccountCredential;
        var tracksListResponse = await tracksList.ExecuteAsync();
        var tracks = tracksListResponse.Tracks;
        
        _logger.LogDebug($"Tracks available");
        foreach (var track in tracks)
        {
            _logger.LogDebug($"Track: {track.TrackValue}");
            var releases = track.Releases;
            _logger.LogDebug("Releases: ");
            foreach (var rel in releases)
            {
                var version = rel.VersionCodes != null ? string.Join(", ", rel.VersionCodes) : string.Empty;
                _logger.LogDebug($"{rel.Name} version: {version} - Status: {rel.Status}");
            }
        }

        var oauthToken = _serviceAccountCredential
            .GetAccessTokenForRequestAsync(AndroidPublisherService.Scope.Androidpublisher).Result;
        var upload = service.Edits.Bundles.Upload(packageName, editId, aabStream, "application/octet-stream");
        upload.OauthToken = oauthToken;
        var uri = await upload.InitiateSessionAsync();
        _logger.LogDebug($"Upload Uri: {uri}");

        var uploadProgress = await upload.UploadAsync();
        
        if (uploadProgress == null || uploadProgress.Exception != null)
        {
            var message = $"Failed to upload. Error: {uploadProgress?.Exception}";
            _logger.LogError(message);
            throw new Exception(message);
        }
        
        _logger.LogInformation($"Upload {uploadProgress.Status}");

        var releaseNotes = _releaseNotesFactory.Create();

        var releaseTrack = new Track
        {
            Releases = new List<TrackRelease>(new[]
            {
                new TrackRelease
                {
                    Name = $"{upload?.ResponseBody?.VersionCode}",
                    Status = "completed",
                    InAppUpdatePriority = 5,
                    //CountryTargeting = new CountryTargeting {IncludeRestOfWorld = true}, // only available for staged releases
                    ReleaseNotes = releaseNotes,
                    VersionCodes = new List<long?>(new[] {(long?) upload?.ResponseBody?.VersionCode})
                }
            })
        };
        var tracksUpdate = service.Edits.Tracks.Update(releaseTrack, packageName, editId, trackName);
        tracksUpdate.Credential = _serviceAccountCredential;
        var tracksUpdateResponse = await tracksUpdate.ExecuteAsync();
        _logger.LogInformation($"Track {tracksUpdateResponse.TrackValue}");

        return editId;
    }
    
    private async Task Commit(AndroidPublisherService service, string editId, string packageName)
    {
        var commitResult = service.Edits.Commit(packageName, editId);
        commitResult.Credential = _serviceAccountCredential;
        var editCommitResponse = await commitResult.ExecuteAsync();
        _logger.LogInformation($"{editCommitResponse.Id} has been committed");
    }
}