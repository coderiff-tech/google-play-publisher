using Google.Apis.AndroidPublisher.v3.Data;

namespace Roundev.GooglePlayPublisher.Factories;

public class ReleaseNotesFactory
{
    public IList<LocalizedText> Create()
    {
        var result = new List<LocalizedText>(new[]
        {
            new LocalizedText {Language = "en-US", Text = "foo"},
            new LocalizedText {Language = "es-ES", Text = "foo"}
        });
        
        return result;
    }
}