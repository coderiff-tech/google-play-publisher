using System.Text;
using System.Text.RegularExpressions;
using Google.Apis.AndroidPublisher.v3;
using Google.Apis.Auth.OAuth2;

namespace Roundev.GooglePlayPublisher.Factories;

public class ServiceAccountCredentialsFactory
{
    private readonly IConfiguration _configuration;

    public ServiceAccountCredentialsFactory(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    
    public ServiceAccountCredential CreateFromJsonProperties()
    {
        var jsonCredentials = new JsonCredentials();
        _configuration.Bind("ServiceAccount", jsonCredentials);
        
        var credential =
            GoogleCredential
                .FromJsonParameters(new JsonCredentialParameters
                {
                    PrivateKey = jsonCredentials.PrivateKey,
                    ClientEmail = jsonCredentials.ClientEmail,
                    ProjectId = jsonCredentials.ProjectId,
                    Type = jsonCredentials.Type,
                    PrivateKeyId = jsonCredentials.PrivateKeyId,
                    ClientId = jsonCredentials.ClientId
                })
                .CreateScoped(AndroidPublisherService.Scope.Androidpublisher);
        
        var serviceAccountCredential = credential.UnderlyingCredential as ServiceAccountCredential;
        return serviceAccountCredential ?? throw new InvalidOperationException("Could not instantiate service account credential");
    }

    
    
    private class JsonCredentials
    {
        public string Type { get; init; } = string.Empty;
        public string PrivateKeyId { get; init; } = string.Empty;
        public string PrivateKeyHex { get; init; } = string.Empty;
        public string PrivateKey => ToJsonValue(PrivateKeyHex);
        public string ClientEmail { get; init; } = string.Empty;
        public string ProjectId { get; init; } = string.Empty;
        public string ClientId { get; init; } = string.Empty;

        private string ToJsonValue(string hexadecimalValue)
        {
            var hexadecimalValueBytes = Convert.FromHexString(hexadecimalValue);
            var result = Encoding.UTF8.GetString(hexadecimalValueBytes);
            var unescapedResult = Regex.Unescape(result);
            return unescapedResult;
        }
    }
}